{{- define "nginx.conf" }}
user  nginx;
worker_processes  auto;
timer_resolution 100ms;
worker_rlimit_nofile 500000;
error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  worker_connections 12500;
  use epoll;
  multi_accept on;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;
  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                        '$status $body_bytes_sent $request_time "$http_referer" '
                        '"$http_user_agent" "$http_x_forwarded_for"';
  access_log  /var/log/nginx/access.log  main;

  {{ .Values.web.config.http | indent 2}}

  include /etc/nginx/conf.d/*.conf;
}
{{- end }}

{{- define "nginx_server.conf" }}
{{ .Values.web.config.server }}
{{- end }}

{{- define "hook_script.sh" }}
#!/bin/sh
set -e
{{- range $step := .Values.hook.steps }}
echo "[$(date +"%Y-%m-%d %H:%M:%S")] hook.INFO: Step {{ $step.name }}"
{{ $step.command }}
{{- end }}
{{- end }}

{{- define "entrypoint.sh" }}
#!/bin/sh
set -e
echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Entrypoint begin"
{{- range $step := .Values.app.entrypoint.steps }}
echo "[$(date +"%Y-%m-%d %H:%M:%S")] hook.INFO: Step {{ $step.name }}"
{{ $step.command }}
{{- end }}
echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Entrypoint end"
exec "$@"
{{- end }}
